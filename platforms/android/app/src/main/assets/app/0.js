(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[0],{

/***/ "./app/search/minlen.validator.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinLenDirective", function() { return MinLenDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/forms/fesm5/forms.js");


var MinLenDirective = /** @class */ (function () {
    function MinLenDirective() {
        //
    }
    MinLenDirective_1 = MinLenDirective;
    MinLenDirective.prototype.validate = function (control) {
        return !control.value || control.value.length >= (this.minlen
            || 2) ? null : { minlen: true };
    };
    var MinLenDirective_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MinLenDirective.prototype, "minlen", void 0);
    MinLenDirective = MinLenDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: "[minlen]",
            providers: [{
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: MinLenDirective_1,
                    multi: true
                }]
        }),
        __metadata("design:paramtypes", [])
    ], MinLenDirective);
    return MinLenDirective;
}());



/***/ }),

/***/ "./app/search/search-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFormComponent", function() { return SearchFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");

var SearchFormComponent = /** @class */ (function () {
    function SearchFormComponent() {
        this.textFieldValue = "";
        this.search = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SearchFormComponent.prototype.ngOnInit = function () {
        this.textFieldValue = this.inicial;
    };
    SearchFormComponent.prototype.onButtonTap = function () {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], SearchFormComponent.prototype, "search", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], SearchFormComponent.prototype, "inicial", void 0);
    SearchFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "SearchForm",
            template: "\n  \n    <FlexboxLayout flexDirection=\"row\"> \n    <TextField #texto=\"ngModel\" [(ngModel)]=\"textFieldValue\"\n    hint=\"Ingresar texto...\" required minlen=\"4\">\n    </TextField>\n    <Label *ngIf=\"texto.hasError('required')\" text=\"*\"></Label>\n    <Label *ngIf=\"!texto.hasError('required')\n    && texto.hasError('minlen')\" text=\"4+\">\n    </Label>\n    </FlexboxLayout>\n    <Button text=\"Buscar!\" (tap)=\"onButtonTap()\" *ngIf=\"texto.valid\"></Button> \n\n    "
        })
    ], SearchFormComponent);
    return SearchFormComponent;
}());



/***/ }),

/***/ "./app/search/search-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchRoutingModule", function() { return SearchRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/search/search.component.ts");



var routes = [
    { path: "", component: _search_component__WEBPACK_IMPORTED_MODULE_2__["SearchComponent"] }
];
var SearchRoutingModule = /** @class */ (function () {
    function SearchRoutingModule() {
    }
    SearchRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], SearchRoutingModule);
    return SearchRoutingModule;
}());



/***/ }),

/***/ "./app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar class=\"action-bar\">\n    <!-- \n    Use the NavigationButton as a side-drawer button in Android\n    because ActionItems are shown on the right side of the ActionBar\n    -->\n    <NavigationButton ios:visibility=\"collapsed\" icon=\"res://menu\" (tap)=\"onDrawerButtonTap()\"></NavigationButton>\n    <!-- \n    Use the ActionItem for IOS with position set to left. Using the\n    NavigationButton as a side-drawer button in iOS is not possible,\n    because its function is to always navigate back in the application.\n    -->\n    <ActionItem icon=\"res://navigation/menu\" android:visibility=\"collapsed\" (tap)=\"onDrawerButtonTap()\" ios.position=\"left\">\n    </ActionItem>\n    <Label class=\"action-bar-title\" text=\"Search\"></Label>\n</ActionBar>\n\n\n<StackLayout class=\"page page-content\" #layout>\n    <Label class=\"page-icon fa\" text=\"&#xf002;\"></Label>\n    <SearchForm (search)=\"buscarAhora($event)\" [inicial]=\"'hola'\"></SearchForm>\n        <!-- <PullToRefresh (refresh)=\"onPull($event)\"> -->\n        <ListView class=\"list-group\" [items]=\"this.resultados\" (itemTap)=\"onItemTap($event)\" style=\"height: 1250px\">\n            <ng-template let-x=\"item\">\n            <FlexboxLayout flexDirection=\"row\" class=\"list-group-item\">\n                <Image src=\"res://icon\" class=\"thumb img-circle\" (longPress) = \"onLongPress(x)\"></Image>\n                <Label [text]=\"x\" class=\"list-group-item-heading\" verticalAlignment=\"center\" style=\"width: 100%\"></Label>                \n                <Button text=\"Borrar\" class=\"btn\" (tap)=\"onDelete(x)\" style=\"width: 25%\"></Button>\n                <Button text=\"Detalle\" class=\"btn\" (tap)=\"onDetalle(x)\" style=\"width: 25%\" ></Button>\n                <Image src=\"res://icon\" class=\"thumb img-circle\" (longPress)=\"onLongPress(x)\" style=\"width: 25%\"></Image>\n            </FlexboxLayout>\n            </ng-template>\n        </ListView>\n                <!-- </PullToRefresh> -->\n</StackLayout>\n"

/***/ }),

/***/ "./app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/domain/noticias.services.ts");
/* harmony import */ var nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-toasts/index.js");
/* harmony import */ var nativescript_toasts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/domain/noticias-state.model.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-social-share/social-share.js");
/* harmony import */ var nativescript_social_share__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__);







var SearchComponent = /** @class */ (function () {
    function SearchComponent(noticias, store) {
        this.noticias = noticias;
        this.store = store;
        // Use the component constructor to inject providers.
    }
    SearchComponent.prototype.ngOnInit = function () {
        // this.noticias.agregar("hola 1");
        // this.noticias.agregar("hola 2");
        // this.noticias.agregar("hola 3");
        // console.log("asfaf");
        // console.log({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.dir({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.log([1, 2, 3]);
        // console.dir([4, 5, 6]);
        // Init your component properties here.
        this.store.select(function (state) { return state.noticias.sugerida; })
            .subscribe(function (data) {
            var f = data;
            if (f != null) {
                nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["show"]({ text: "Sugerimos leer: " + f.titulo, duration: nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["DURATION"].SHORT });
            }
        });
    };
    SearchComponent.prototype.onDrawerButtonTap = function () {
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__["getRootView"]();
        sideDrawer.showDrawer();
    };
    SearchComponent.prototype.onItemTap = function (args) {
        // console.dir(x);
        this.store.dispatch(new _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__["NuevaNoticiaAction"](new _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__["Noticia"](args.view.bindingContext)));
    };
    SearchComponent.prototype.onLongPress = function (s) {
        console.log(s);
        nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__["shareText"](s, "Asunto: Compartido desde el curso!");
    };
    SearchComponent.prototype.buscarAhora = function (s) {
        var _this = this;
        // this.resultados = this.noticias.buscar().filter((x)=> x.indexOf(s)>=0);
        // console.log(s);
        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 300,
        //     delay: 150        }));
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then(function (r) {
            console.log("resultados buscarAhora " + JSON.stringify(r));
            _this.resultados = r;
        }, function (e) {
            console.log("Error buscarAhora " + e);
            nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["show"]({ text: "Error en la busqueda", duration: nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["DURATION"].SHORT });
        });
    };
    SearchComponent.prototype.onDelete = function (item) {
        this.resultados.splice(item, 1);
        alert('Se elimino el item ' + item);
    };
    SearchComponent.prototype.onPull = function (arg) {
        var _this = this;
        var contador = this.resultados.length + 1;
        var saludos = "Hola " + contador;
        console.log(arg);
        var pullRefresh = arg.object;
        setTimeout(function () {
            _this.resultados.push(saludos);
            pullRefresh.refreshing = false;
        }, 1000);
    };
    SearchComponent.prototype.onDetalle = function (item) {
        alert('Mostrar los detalles del elemento ' + item);
    };
    SearchComponent.ctorParameters = function () { return [
        { type: _domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("layout", null),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SearchComponent.prototype, "layout", void 0);
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Search",
            template: __webpack_require__("./app/search/search.component.html")
        }),
        __metadata("design:paramtypes", [_domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./app/search/search.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModule", function() { return SearchModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/common.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/search/search-routing.module.ts");
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/search/search.component.ts");
/* harmony import */ var _search_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./app/search/search-form.component.ts");
/* harmony import */ var _search_minlen_validator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./app/search/minlen.validator.ts");







var SearchModule = /** @class */ (function () {
    function SearchModule() {
    }
    SearchModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _search_routing_module__WEBPACK_IMPORTED_MODULE_3__["SearchRoutingModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__["NativeScriptFormsModule"]
            ],
            declarations: [
                _search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"],
                _search_form_component__WEBPACK_IMPORTED_MODULE_5__["SearchFormComponent"],
                _search_minlen_validator__WEBPACK_IMPORTED_MODULE_6__["MinLenDirective"]
            ],
            //providers: [NoticiasService],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], SearchModule);
    return SearchModule;
}());



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL21pbmxlbi52YWxpZGF0b3IudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gtZm9ybS5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gtcm91dGluZy5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL2FwcC9zZWFyY2gvc2VhcmNoLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpRDtBQUM0QjtBQVM3RTtJQUVJO1FBQ0ksRUFBRTtJQUNOLENBQUM7d0JBSlEsZUFBZTtJQUt4QixrQ0FBUSxHQUFSLFVBQVMsT0FBd0I7UUFDN0IsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtlQUN0RCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztJQUN4QyxDQUFDOztJQVBRO1FBQVIsMkRBQUssRUFBRTs7bURBQWdCO0lBRGYsZUFBZTtRQVAzQiwrREFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFVBQVU7WUFDcEIsU0FBUyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLDREQUFhLEVBQUUsV0FBVyxFQUFFLGlCQUFlO29CQUNwRCxLQUFLLEVBQUUsSUFBSTtpQkFDZCxDQUFDO1NBQ0wsQ0FBQzs7T0FDVyxlQUFlLENBUzNCO0lBQUQsc0JBQUM7Q0FBQTtBQVQyQjs7Ozs7Ozs7O0FDVjVCO0FBQUE7QUFBQTtBQUE4RTtBQXFCOUU7SUFuQkE7UUFvQkksbUJBQWMsR0FBVyxFQUFFLENBQUM7UUFDbEIsV0FBTSxHQUF5QixJQUFJLDBEQUFZLEVBQUUsQ0FBQztJQWVoRSxDQUFDO0lBWkcsc0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU87SUFFdEMsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNsQyxJQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDMUM7SUFDTCxDQUFDO0lBYlM7UUFBVCw0REFBTSxFQUFFO2tDQUFTLDBEQUFZO3VEQUE4QjtJQUNuRDtRQUFSLDJEQUFLLEVBQUU7O3dEQUFpQjtJQUhoQixtQkFBbUI7UUFuQi9CLCtEQUFTLENBQUU7WUFDUixRQUFRLEVBQUUsWUFBWTtZQUV0QixRQUFRLEVBQUUscWVBYVQ7U0FDSixDQUFDO09BRVcsbUJBQW1CLENBaUIvQjtJQUFELDBCQUFDO0NBQUE7QUFqQitCOzs7Ozs7Ozs7QUNyQmhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUU4QjtBQUVsQjtBQUVyRCxJQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlFQUFlLEVBQUU7Q0FDM0MsQ0FBQztBQU1GO0lBQUE7SUFBbUMsQ0FBQztJQUF2QixtQkFBbUI7UUFKL0IsOERBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxPQUFPLEVBQUUsQ0FBQyxvRkFBd0IsQ0FBQztTQUN0QyxDQUFDO09BQ1csbUJBQW1CLENBQUk7SUFBRCwwQkFBQztDQUFBO0FBQUo7Ozs7Ozs7O0FDZGhDLGc0QkFBZzRCLGtvQzs7Ozs7Ozs7QUNBaDRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF3RTtBQUVwQjtBQUVVO0FBQ2pCO0FBQ2dDO0FBRWxDO0FBQ2M7QUFRekQ7SUFJSSx5QkFBb0IsUUFBeUIsRUFDakMsS0FBc0I7UUFEZCxhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUNqQyxVQUFLLEdBQUwsS0FBSyxDQUFpQjtRQUM5QixxREFBcUQ7SUFDekQsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDSSxtQ0FBbUM7UUFDbkMsbUNBQW1DO1FBQ25DLG1DQUFtQztRQUNuQyx3QkFBd0I7UUFDeEIsK0RBQStEO1FBQy9ELCtEQUErRDtRQUMvRCwwQkFBMEI7UUFDMUIsMEJBQTBCO1FBQzFCLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLEtBQUssSUFBSyxZQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBdkIsQ0FBdUIsQ0FBQzthQUNoRCxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ1osSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ2YsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUNYLHdEQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsNERBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZGO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCO1FBQ0ksSUFBTSxVQUFVLEdBQWtCLHdFQUFlLEVBQUUsQ0FBQztRQUNwRCxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxJQUFJO1FBQ1Ysa0JBQWtCO1FBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksK0VBQWtCLENBQUMsSUFBSSxvRUFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFDRCxxQ0FBVyxHQUFYLFVBQVksQ0FBQztRQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDZixtRUFBcUIsQ0FBQyxDQUFDLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBR0gscUNBQVcsR0FBWCxVQUFZLENBQVM7UUFBckIsaUJBb0JDO1FBbkJHLDBFQUEwRTtRQUMxRSxrQkFBa0I7UUFDbEIsa0RBQWtEO1FBQ2xELG1CQUFtQjtRQUNuQiwwQ0FBMEM7UUFDMUMscUJBQXFCO1FBQ3JCLGlCQUFpQjtRQUNqQixpQ0FBaUM7UUFDakMsMkNBQTJDO1FBQzNDLHFCQUFxQjtRQUNyQiw2QkFBNkI7UUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBSztZQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRCxLQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUN4QixDQUFDLEVBQUUsVUFBQyxDQUFDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQyx3REFBVSxDQUFDLEVBQUMsSUFBSSxFQUFFLHNCQUFzQixFQUFFLFFBQVEsRUFBRSw0REFBYyxDQUFDLEtBQUssRUFBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELGtDQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUV4QyxDQUFDO0lBQ0QsZ0NBQU0sR0FBTixVQUFPLEdBQUc7UUFBVixpQkFVQztRQVRHLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUMxQyxJQUFJLE9BQU8sR0FBRyxPQUFPLEdBQUcsUUFBUSxDQUFDO1FBRWpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUMvQixVQUFVLENBQUM7WUFDUCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5QixXQUFXLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsbUNBQVMsR0FBVCxVQUFVLElBQUk7UUFDVixLQUFLLENBQUMsb0NBQW9DLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Z0JBL0U2Qix5RUFBZTtnQkFDMUIsaURBQUs7O0lBSEU7UUFBekIsK0RBQVMsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDO2tDQUFVLHdEQUFVO21EQUFBO0lBRnBDLGVBQWU7UUFOM0IsK0RBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLG1FQUFzQztTQUd6QyxDQUFDO3lDQUtnQyx5RUFBZTtZQUMxQixpREFBSztPQUxmLGVBQWUsQ0FxRjNCO0lBQUQsc0JBQUM7Q0FBQTtBQXJGMkI7Ozs7Ozs7OztBQ2pCNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyRDtBQUNZO0FBQ0Y7QUFFUDtBQUNUO0FBRVM7QUFDRDtBQWtCN0Q7SUFBQTtJQUE0QixDQUFDO0lBQWhCLFlBQVk7UUFoQnhCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0ZBQXdCO2dCQUN4QiwwRUFBbUI7Z0JBQ25CLGtGQUF1QjthQUMxQjtZQUNELFlBQVksRUFBRTtnQkFDVixpRUFBZTtnQkFDZiwwRUFBbUI7Z0JBQ25CLHdFQUFlO2FBQ2xCO1lBQ0QsK0JBQStCO1lBQy9CLE9BQU8sRUFBRTtnQkFDTCw4REFBZ0I7YUFDbkI7U0FDSixDQUFDO09BQ1csWUFBWSxDQUFJO0lBQUQsbUJBQUM7Q0FBQTtBQUFKIiwiZmlsZSI6IjAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgTkdfVkFMSURBVE9SUywgVmFsaWRhdG9yIH0gZnJvbSAgIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuQERpcmVjdGl2ZSh7XG4gICAgc2VsZWN0b3I6IFwiW21pbmxlbl1cIixcbiAgICBwcm92aWRlcnM6IFt7XG4gICAgICAgIHByb3ZpZGU6IE5HX1ZBTElEQVRPUlMsIHVzZUV4aXN0aW5nOiBNaW5MZW5EaXJlY3RpdmUsXG4gICAgICAgIG11bHRpOiB0cnVlXG4gICAgfV1cbn0pXG5leHBvcnQgY2xhc3MgTWluTGVuRGlyZWN0aXZlIGltcGxlbWVudHMgVmFsaWRhdG9yIHtcbiAgICBASW5wdXQoKSBtaW5sZW46IHN0cmluZztcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgLy9cbiAgICB9XG4gICAgdmFsaWRhdGUoY29udHJvbDogQWJzdHJhY3RDb250cm9sKTogeyBba2V5OiBzdHJpbmddOiBhbnkgfSB7XG4gICAgICAgIHJldHVybiAhY29udHJvbC52YWx1ZSB8fCBjb250cm9sLnZhbHVlLmxlbmd0aCA+PSAodGhpcy5taW5sZW5cbiAgICAgICAgICAgIHx8IDIpID8gbnVsbCA6IHsgbWlubGVuOiB0cnVlIH07XG4gICAgfVxufSIsImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBDb21wb25lbnQgKHtcbiAgICBzZWxlY3RvcjogXCJTZWFyY2hGb3JtXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZTogYFxuICBcbiAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVwicm93XCI+IFxuICAgIDxUZXh0RmllbGQgI3RleHRvPVwibmdNb2RlbFwiIFsobmdNb2RlbCldPVwidGV4dEZpZWxkVmFsdWVcIlxuICAgIGhpbnQ9XCJJbmdyZXNhciB0ZXh0by4uLlwiIHJlcXVpcmVkIG1pbmxlbj1cIjRcIj5cbiAgICA8L1RleHRGaWVsZD5cbiAgICA8TGFiZWwgKm5nSWY9XCJ0ZXh0by5oYXNFcnJvcigncmVxdWlyZWQnKVwiIHRleHQ9XCIqXCI+PC9MYWJlbD5cbiAgICA8TGFiZWwgKm5nSWY9XCIhdGV4dG8uaGFzRXJyb3IoJ3JlcXVpcmVkJylcbiAgICAmJiB0ZXh0by5oYXNFcnJvcignbWlubGVuJylcIiB0ZXh0PVwiNCtcIj5cbiAgICA8L0xhYmVsPlxuICAgIDwvRmxleGJveExheW91dD5cbiAgICA8QnV0dG9uIHRleHQ9XCJCdXNjYXIhXCIgKHRhcCk9XCJvbkJ1dHRvblRhcCgpXCIgKm5nSWY9XCJ0ZXh0by52YWxpZFwiPjwvQnV0dG9uPiBcblxuICAgIGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBTZWFyY2hGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICB0ZXh0RmllbGRWYWx1ZTogc3RyaW5nID0gXCJcIjtcbiAgICBAT3V0cHV0KCkgc2VhcmNoOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBASW5wdXQoKSBpbmljaWFsOiBzdHJpbmc7XG5cbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy50ZXh0RmllbGRWYWx1ZSA9IHRoaXMuaW5pY2lhbFxuICAgICAgICBcbiAgICB9XG5cbiAgICBvbkJ1dHRvblRhcCgpOiB2b2lkIHtcbiAgICAgICAgY29uc29sZS5sb2cgKHRoaXMudGV4dEZpZWxkVmFsdWUpO1xuICAgICAgICBpZih0aGlzLnRleHRGaWVsZFZhbHVlLmxlbmd0aD4yKSB7IFxuICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdCAodGhpcy50ZXh0RmllbGRWYWx1ZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbn0iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbmltcG9ydCB7IFNlYXJjaENvbXBvbmVudCB9IGZyb20gXCIuL3NlYXJjaC5jb21wb25lbnRcIjtcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IFNlYXJjaENvbXBvbmVudCB9XG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXG4gICAgZXhwb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoUm91dGluZ01vZHVsZSB7IH1cbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXG4gICAgPCEtLSBcXG4gICAgVXNlIHRoZSBOYXZpZ2F0aW9uQnV0dG9uIGFzIGEgc2lkZS1kcmF3ZXIgYnV0dG9uIGluIEFuZHJvaWRcXG4gICAgYmVjYXVzZSBBY3Rpb25JdGVtcyBhcmUgc2hvd24gb24gdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIEFjdGlvbkJhclxcbiAgICAtLT5cXG4gICAgPE5hdmlnYXRpb25CdXR0b24gaW9zOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgaWNvbj1cXFwicmVzOi8vbWVudVxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiPjwvTmF2aWdhdGlvbkJ1dHRvbj5cXG4gICAgPCEtLSBcXG4gICAgVXNlIHRoZSBBY3Rpb25JdGVtIGZvciBJT1Mgd2l0aCBwb3NpdGlvbiBzZXQgdG8gbGVmdC4gVXNpbmcgdGhlXFxuICAgIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gaU9TIGlzIG5vdCBwb3NzaWJsZSxcXG4gICAgYmVjYXVzZSBpdHMgZnVuY3Rpb24gaXMgdG8gYWx3YXlzIG5hdmlnYXRlIGJhY2sgaW4gdGhlIGFwcGxpY2F0aW9uLlxcbiAgICAtLT5cXG4gICAgPEFjdGlvbkl0ZW0gaWNvbj1cXFwicmVzOi8vbmF2aWdhdGlvbi9tZW51XFxcIiBhbmRyb2lkOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiIGlvcy5wb3NpdGlvbj1cXFwibGVmdFxcXCI+XFxuICAgIDwvQWN0aW9uSXRlbT5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJhY3Rpb24tYmFyLXRpdGxlXFxcIiB0ZXh0PVxcXCJTZWFyY2hcXFwiPjwvTGFiZWw+XFxuPC9BY3Rpb25CYXI+XFxuXFxuXFxuPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJwYWdlIHBhZ2UtY29udGVudFxcXCIgI2xheW91dD5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlLWljb24gZmFcXFwiIHRleHQ9XFxcIiYjeGYwMDI7XFxcIj48L0xhYmVsPlxcbiAgICA8U2VhcmNoRm9ybSAoc2VhcmNoKT1cXFwiYnVzY2FyQWhvcmEoJGV2ZW50KVxcXCIgW2luaWNpYWxdPVxcXCInaG9sYSdcXFwiPjwvU2VhcmNoRm9ybT5cXG4gICAgICAgIDwhLS0gPFB1bGxUb1JlZnJlc2ggKHJlZnJlc2gpPVxcXCJvblB1bGwoJGV2ZW50KVxcXCI+IC0tPlxcbiAgICAgICAgPExpc3RWaWV3IGNsYXNzPVxcXCJsaXN0LWdyb3VwXFxcIiBbaXRlbXNdPVxcXCJ0aGlzLnJlc3VsdGFkb3NcXFwiIChpdGVtVGFwKT1cXFwib25JdGVtVGFwKCRldmVudClcXFwiIHN0eWxlPVxcXCJoZWlnaHQ6IDEyNTBweFxcXCI+XFxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlIGxldC14PVxcXCJpdGVtXFxcIj5cXG4gICAgICAgICAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVxcXCJyb3dcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwLWl0ZW1cXFwiPlxcbiAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJyZXM6Ly9pY29uXFxcIiBjbGFzcz1cXFwidGh1bWIgaW1nLWNpcmNsZVxcXCIgKGxvbmdQcmVzcykgPSBcXFwib25Mb25nUHJlc3MoeClcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCBbdGV4dF09XFxcInhcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwLWl0ZW0taGVhZGluZ1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgc3R5bGU9XFxcIndpZHRoOiAxMDAlXFxcIj48L0xhYmVsPiAgICAgICAgICAgICAgICBcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJCb3JyYXJcXFwiIGNsYXNzPVxcXCJidG5cXFwiICh0YXApPVxcXCJvbkRlbGV0ZSh4KVxcXCIgc3R5bGU9XFxcIndpZHRoOiAyNSVcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkRldGFsbGVcXFwiIGNsYXNzPVxcXCJidG5cXFwiICh0YXApPVxcXCJvbkRldGFsbGUoeClcXFwiIHN0eWxlPVxcXCJ3aWR0aDogMjUlXFxcIiA+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcInJlczovL2ljb25cXFwiIGNsYXNzPVxcXCJ0aHVtYiBpbWctY2lyY2xlXFxcIiAobG9uZ1ByZXNzKT1cXFwib25Mb25nUHJlc3MoeClcXFwiIHN0eWxlPVxcXCJ3aWR0aDogMjUlXFxcIj48L0ltYWdlPlxcbiAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxcbiAgICAgICAgPC9MaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgPCEtLSA8L1B1bGxUb1JlZnJlc2g+IC0tPlxcbjwvU3RhY2tMYXlvdXQ+XFxuXCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xuaW1wb3J0ICogYXMgYXBwIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCI7XG5pbXBvcnQge0NvbG9yLCBWaWV3fSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9jb3JlL3ZpZXcvdmlld1wiXG5pbXBvcnQgeyBOb3RpY2lhc1NlcnZpY2UgfSBmcm9tIFwiLi4vZG9tYWluL25vdGljaWFzLnNlcnZpY2VzXCI7XG5pbXBvcnQgKiBhcyBUb2FzdCBmcm9tIFwibmF0aXZlc2NyaXB0LXRvYXN0c1wiO1xuaW1wb3J0IHsgTm90aWNpYSwgTnVldmFOb3RpY2lhQWN0aW9uIH0gZnJvbSBcIi4uL2RvbWFpbi9ub3RpY2lhcy1zdGF0ZS5tb2RlbFwiO1xuaW1wb3J0IHsgQXBwU3RhdGUgfSBmcm9tIFwiLi4vYXBwLm1vZHVsZVwiO1xuaW1wb3J0IHsgU3RvcmUsIFN0YXRlIH0gZnJvbSBcIkBuZ3J4L3N0b3JlXCI7XG5pbXBvcnQgKiBhcyBTb2NpYWxTaGFyZSBmcm9tIFwibmF0aXZlc2NyaXB0LXNvY2lhbC1zaGFyZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJTZWFyY2hcIixcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3NlYXJjaC5jb21wb25lbnQuaHRtbFwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWRcbiAgICAvL3Byb3ZpZGVyczogW05vdGljaWFzU2VydmljZV1cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICByZXN1bHRhZG9zOiBBcnJheTxzdHJpbmc+O1xuICAgIEBWaWV3Q2hpbGQoXCJsYXlvdXRcIixudWxsKSBsYXlvdXQgOiBFbGVtZW50UmVmXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG5vdGljaWFzOiBOb3RpY2lhc1NlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgc3RvcmU6IFN0b3JlPEFwcFN0YXRlPikge1xuICAgICAgICAvLyBVc2UgdGhlIGNvbXBvbmVudCBjb25zdHJ1Y3RvciB0byBpbmplY3QgcHJvdmlkZXJzLlxuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICAvLyB0aGlzLm5vdGljaWFzLmFncmVnYXIoXCJob2xhIDFcIik7XG4gICAgICAgIC8vIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGEgMlwiKTtcbiAgICAgICAgLy8gdGhpcy5ub3RpY2lhcy5hZ3JlZ2FyKFwiaG9sYSAzXCIpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhcImFzZmFmXCIpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh7bm9tYnJlOiB7bm9tYnJlOiB7bm9tYnJlOiB7bm9tYnJlOiBcInBlcGVcIn19fX0pO1xuICAgICAgICAvLyBjb25zb2xlLmRpcih7bm9tYnJlOiB7bm9tYnJlOiB7bm9tYnJlOiB7bm9tYnJlOiBcInBlcGVcIn19fX0pO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhbMSwgMiwgM10pO1xuICAgICAgICAvLyBjb25zb2xlLmRpcihbNCwgNSwgNl0pO1xuICAgICAgICAvLyBJbml0IHlvdXIgY29tcG9uZW50IHByb3BlcnRpZXMgaGVyZS5cbiAgICAgICAgdGhpcy5zdG9yZS5zZWxlY3QoKHN0YXRlKSA9PiBzdGF0ZS5ub3RpY2lhcy5zdWdlcmlkYSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBmID0gZGF0YTtcbiAgICAgICAgICAgICAgICBpZiAoZiAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIFRvYXN0LnNob3coeyB0ZXh0OiBcIlN1Z2VyaW1vcyBsZWVyOiBcIiArIGYudGl0dWxvLCBkdXJhdGlvbjogVG9hc3QuRFVSQVRJT04uU0hPUlQgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgb25EcmF3ZXJCdXR0b25UYXAoKTogdm9pZCB7XG4gICAgICAgIGNvbnN0IHNpZGVEcmF3ZXIgPSA8UmFkU2lkZURyYXdlcj5hcHAuZ2V0Um9vdFZpZXcoKTtcbiAgICAgICAgc2lkZURyYXdlci5zaG93RHJhd2VyKCk7XG4gICAgfVxuXG4gICAgb25JdGVtVGFwKGFyZ3MpOiB2b2lkIHtcbiAgICAgICAgLy8gY29uc29sZS5kaXIoeCk7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IE51ZXZhTm90aWNpYUFjdGlvbihuZXcgTm90aWNpYShhcmdzLnZpZXcuYmluZGluZ0NvbnRleHQpKSk7XG4gICAgfVxuICAgIG9uTG9uZ1ByZXNzKHMpIHtcbiAgICAgICAgY29uc29sZS5sb2cocyk7XG4gICAgICAgIFNvY2lhbFNoYXJlLnNoYXJlVGV4dChzLCBcIkFzdW50bzogQ29tcGFydGlkbyBkZXNkZSBlbCBjdXJzbyFcIik7XG4gICAgICB9XG4gICAgXG5cbiAgICBidXNjYXJBaG9yYShzOiBzdHJpbmcpIHtcbiAgICAgICAgLy8gdGhpcy5yZXN1bHRhZG9zID0gdGhpcy5ub3RpY2lhcy5idXNjYXIoKS5maWx0ZXIoKHgpPT4geC5pbmRleE9mKHMpPj0wKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2cocyk7XG4gICAgICAgIC8vIGNvbnN0IGxheW91dCA9IDxWaWV3PnRoaXMubGF5b3V0Lm5hdGl2ZUVsZW1lbnQ7XG4gICAgICAgIC8vIGxheW91dC5hbmltYXRlKHtcbiAgICAgICAgLy8gICAgIGJhY2tncm91bmRDb2xvcjogbmV3IENvbG9yKFwiYmx1ZVwiKSxcbiAgICAgICAgLy8gICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgIC8vICAgICBkZWxheTogMTUwXG4gICAgICAgIC8vIH0pLnRoZW4oKCkgPT4gbGF5b3V0LmFuaW1hdGUoe1xuICAgICAgICAvLyAgICAgYmFja2dyb3VuZENvbG9yOiBuZXcgQ29sb3IoXCJ3aGl0ZVwiKSxcbiAgICAgICAgLy8gICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgIC8vICAgICBkZWxheTogMTUwICAgICAgICB9KSk7XG4gICAgICAgIGNvbnNvbGUuZGlyKFwiYnVzY2FyQWhvcmFcIitzKTtcbiAgICAgICAgdGhpcy5ub3RpY2lhcy5idXNjYXIocykudGhlbigocjphbnkpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmVzdWx0YWRvcyBidXNjYXJBaG9yYSBcIisgSlNPTi5zdHJpbmdpZnkocikpO1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRhZG9zID0gcjtcbiAgICAgICAgfSwgKGUpPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBidXNjYXJBaG9yYSBcIitlKTtcbiAgICAgICAgICAgIFRvYXN0LnNob3coe3RleHQ6IFwiRXJyb3IgZW4gbGEgYnVzcXVlZGFcIiwgZHVyYXRpb246IFRvYXN0LkRVUkFUSU9OLlNIT1JUfSlcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIG9uRGVsZXRlKGl0ZW0pOiB2b2lkIHtcbiAgICAgICAgdGhpcy5yZXN1bHRhZG9zLnNwbGljZShpdGVtLCAxKTtcbiAgICAgICAgYWxlcnQoJ1NlIGVsaW1pbm8gZWwgaXRlbSAnICsgaXRlbSk7XG5cbiAgICB9XG4gICAgb25QdWxsKGFyZykge1xuICAgICAgICBsZXQgY29udGFkb3IgPSB0aGlzLnJlc3VsdGFkb3MubGVuZ3RoICsgMTtcbiAgICAgICAgbGV0IHNhbHVkb3MgPSBcIkhvbGEgXCIgKyBjb250YWRvcjtcblxuICAgICAgICBjb25zb2xlLmxvZyhhcmcpO1xuICAgICAgICBjb25zdCBwdWxsUmVmcmVzaCA9IGFyZy5vYmplY3Q7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRhZG9zLnB1c2goc2FsdWRvcyk7XG4gICAgICAgICAgICBwdWxsUmVmcmVzaC5yZWZyZXNoaW5nID0gZmFsc2U7XG4gICAgICAgIH0sIDEwMDApO1xuICAgIH1cblxuICAgIG9uRGV0YWxsZShpdGVtKTogdm9pZCB7XG4gICAgICAgIGFsZXJ0KCdNb3N0cmFyIGxvcyBkZXRhbGxlcyBkZWwgZWxlbWVudG8gJyArIGl0ZW0pO1xuICAgIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vblwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcblxuaW1wb3J0IHsgU2VhcmNoUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL3NlYXJjaC1yb3V0aW5nLm1vZHVsZVwiO1xuaW1wb3J0IHsgU2VhcmNoQ29tcG9uZW50IH0gZnJvbSBcIi4vc2VhcmNoLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTm90aWNpYXNTZXJ2aWNlIH0gZnJvbSBcIi4uL2RvbWFpbi9ub3RpY2lhcy5zZXJ2aWNlc1wiO1xuaW1wb3J0IHsgU2VhcmNoRm9ybUNvbXBvbmVudCB9IGZyb20gXCIuL3NlYXJjaC1mb3JtLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTWluTGVuRGlyZWN0aXZlIH0gZnJvbSBcIi4uL3NlYXJjaC9taW5sZW4udmFsaWRhdG9yXCI7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXG4gICAgICAgIFNlYXJjaFJvdXRpbmdNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgU2VhcmNoQ29tcG9uZW50LFxuICAgICAgICBTZWFyY2hGb3JtQ29tcG9uZW50LFxuICAgICAgICBNaW5MZW5EaXJlY3RpdmVcbiAgICBdLFxuICAgIC8vcHJvdmlkZXJzOiBbTm90aWNpYXNTZXJ2aWNlXSxcbiAgICBzY2hlbWFzOiBbXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaE1vZHVsZSB7IH1cbiJdLCJzb3VyY2VSb290IjoiIn0=