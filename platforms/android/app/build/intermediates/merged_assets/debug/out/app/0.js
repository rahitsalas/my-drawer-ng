(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[0],{

/***/ "./app/search/minlen.validator.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinLenDirective", function() { return MinLenDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/forms/fesm5/forms.js");


var MinLenDirective = /** @class */ (function () {
    function MinLenDirective() {
        //
    }
    MinLenDirective_1 = MinLenDirective;
    MinLenDirective.prototype.validate = function (control) {
        return !control.value || control.value.length >= (this.minlen
            || 2) ? null : { minlen: true };
    };
    var MinLenDirective_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MinLenDirective.prototype, "minlen", void 0);
    MinLenDirective = MinLenDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: "[minlen]",
            providers: [{
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: MinLenDirective_1,
                    multi: true
                }]
        }),
        __metadata("design:paramtypes", [])
    ], MinLenDirective);
    return MinLenDirective;
}());



/***/ }),

/***/ "./app/search/search-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFormComponent", function() { return SearchFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");

var SearchFormComponent = /** @class */ (function () {
    function SearchFormComponent() {
        this.textFieldValue = "";
        this.search = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SearchFormComponent.prototype.ngOnInit = function () {
        this.textFieldValue = this.inicial;
    };
    SearchFormComponent.prototype.onButtonTap = function () {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], SearchFormComponent.prototype, "search", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], SearchFormComponent.prototype, "inicial", void 0);
    SearchFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "SearchForm",
            template: "\n  \n    <FlexboxLayout flexDirection=\"row\"> \n    <TextField #texto=\"ngModel\" [(ngModel)]=\"textFieldValue\"\n    hint=\"Ingresar texto...\" required minlen=\"4\">\n    </TextField>\n    <Label *ngIf=\"texto.hasError('required')\" text=\"*\"></Label>\n    <Label *ngIf=\"!texto.hasError('required')\n    && texto.hasError('minlen')\" text=\"4+\">\n    </Label>\n    </FlexboxLayout>\n    <Button text=\"Buscar!\" (tap)=\"onButtonTap()\" *ngIf=\"texto.valid\"></Button> \n\n    "
        })
    ], SearchFormComponent);
    return SearchFormComponent;
}());



/***/ }),

/***/ "./app/search/search-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchRoutingModule", function() { return SearchRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/search/search.component.ts");



var routes = [
    { path: "", component: _search_component__WEBPACK_IMPORTED_MODULE_2__["SearchComponent"] }
];
var SearchRoutingModule = /** @class */ (function () {
    function SearchRoutingModule() {
    }
    SearchRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], SearchRoutingModule);
    return SearchRoutingModule;
}());



/***/ }),

/***/ "./app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar class=\"action-bar\">\n    <!-- \n    Use the NavigationButton as a side-drawer button in Android\n    because ActionItems are shown on the right side of the ActionBar\n    -->\n    <NavigationButton ios:visibility=\"collapsed\" icon=\"res://menu\" (tap)=\"onDrawerButtonTap()\"></NavigationButton>\n    <!-- \n    Use the ActionItem for IOS with position set to left. Using the\n    NavigationButton as a side-drawer button in iOS is not possible,\n    because its function is to always navigate back in the application.\n    -->\n    <ActionItem icon=\"res://navigation/menu\" android:visibility=\"collapsed\" (tap)=\"onDrawerButtonTap()\" ios.position=\"left\">\n    </ActionItem>\n    <Label class=\"action-bar-title\" text=\"Search\"></Label>\n</ActionBar>\n\n\n<StackLayout class=\"page page-content\" #layout>\n    <Label class=\"page-icon fa\" text=\"&#xf002;\"></Label>\n    <SearchForm (search)=\"buscarAhora($event)\" [inicial]=\"'hola'\"></SearchForm>\n        <!-- <PullToRefresh (refresh)=\"onPull($event)\"> -->\n        <ListView class=\"list-group\" [items]=\"this.resultados\" (itemTap)=\"onItemTap($event)\" style=\"height: 1250px\">\n            <ng-template let-x=\"item\">\n            <FlexboxLayout flexDirection=\"row\" class=\"list-group-item\">\n                <Image src=\"res://icon\" class=\"thumb img-circle\"></Image>\n                <Label [text]=\"x\" class=\"list-group-item-heading\" verticalAlignment=\"center\" style=\"width: 100%\"></Label>                \n                <Button text=\"Borrar\" class=\"btn\" (tap)=\"onDelete(x)\" style=\"width: 25%\"></Button>\n                <Button text=\"Detalle\" class=\"btn\" (tap)=\"onDetalle(x)\" style=\"width: 25%\" ></Button>\n                <Image src=\"res://icon\" class=\"thumb img-circle\" (longPress)=\"onLongPress(x)\" style=\"width: 25%\"></Image>\n            </FlexboxLayout>\n            </ng-template>\n        </ListView>\n                <!-- </PullToRefresh> -->\n</StackLayout>\n"

/***/ }),

/***/ "./app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/domain/noticias.services.ts");
/* harmony import */ var nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-toasts/index.js");
/* harmony import */ var nativescript_toasts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/domain/noticias-state.model.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-social-share/social-share.js");
/* harmony import */ var nativescript_social_share__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__);







var SearchComponent = /** @class */ (function () {
    function SearchComponent(noticias, store) {
        this.noticias = noticias;
        this.store = store;
        // Use the component constructor to inject providers.
    }
    SearchComponent.prototype.ngOnInit = function () {
        // this.noticias.agregar("hola 1");
        // this.noticias.agregar("hola 2");
        // this.noticias.agregar("hola 3");
        // console.log("asfaf");
        // console.log({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.dir({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.log([1, 2, 3]);
        // console.dir([4, 5, 6]);
        // Init your component properties here.
        this.store.select(function (state) { return state.noticias.sugerida; })
            .subscribe(function (data) {
            var f = data;
            if (f != null) {
                nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["show"]({ text: "Sugerimos leer: " + f.titulo, duration: nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["DURATION"].SHORT });
            }
        });
    };
    SearchComponent.prototype.onDrawerButtonTap = function () {
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__["getRootView"]();
        sideDrawer.showDrawer();
    };
    SearchComponent.prototype.onItemTap = function (args) {
        // console.dir(x);
        this.store.dispatch(new _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__["NuevaNoticiaAction"](new _domain_noticias_state_model__WEBPACK_IMPORTED_MODULE_4__["Noticia"](args.view.bindingContext)));
    };
    SearchComponent.prototype.onLongPress = function (s) {
        console.log(s);
        nativescript_social_share__WEBPACK_IMPORTED_MODULE_6__["shareText"](s, "Asunto: Compartido desde el curso!");
    };
    SearchComponent.prototype.buscarAhora = function (s) {
        var _this = this;
        // this.resultados = this.noticias.buscar().filter((x)=> x.indexOf(s)>=0);
        // console.log(s);
        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 300,
        //     delay: 150        }));
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then(function (r) {
            console.log("resultados buscarAhora " + JSON.stringify(r));
            _this.resultados = r;
        }, function (e) {
            console.log("Error buscarAhora " + e);
            nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["show"]({ text: "Error en la busqueda", duration: nativescript_toasts__WEBPACK_IMPORTED_MODULE_3__["DURATION"].SHORT });
        });
    };
    SearchComponent.prototype.onDelete = function (item) {
        this.resultados.splice(item, 1);
        alert('Se elimino el item ' + item);
    };
    SearchComponent.prototype.onPull = function (arg) {
        var _this = this;
        var contador = this.resultados.length + 1;
        var saludos = "Hola " + contador;
        console.log(arg);
        var pullRefresh = arg.object;
        setTimeout(function () {
            _this.resultados.push(saludos);
            pullRefresh.refreshing = false;
        }, 1000);
    };
    SearchComponent.prototype.onDetalle = function (item) {
        alert('Mostrar los detalles del elemento ' + item);
    };
    SearchComponent.ctorParameters = function () { return [
        { type: _domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("layout", null),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SearchComponent.prototype, "layout", void 0);
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Search",
            template: __webpack_require__("./app/search/search.component.html")
        }),
        __metadata("design:paramtypes", [_domain_noticias_services__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./app/search/search.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModule", function() { return SearchModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/common.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/search/search-routing.module.ts");
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/search/search.component.ts");
/* harmony import */ var _search_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./app/search/search-form.component.ts");
/* harmony import */ var _search_minlen_validator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./app/search/minlen.validator.ts");







var SearchModule = /** @class */ (function () {
    function SearchModule() {
    }
    SearchModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _search_routing_module__WEBPACK_IMPORTED_MODULE_3__["SearchRoutingModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_2__["NativeScriptFormsModule"]
            ],
            declarations: [
                _search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"],
                _search_form_component__WEBPACK_IMPORTED_MODULE_5__["SearchFormComponent"],
                _search_minlen_validator__WEBPACK_IMPORTED_MODULE_6__["MinLenDirective"]
            ],
            //providers: [NoticiasService],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], SearchModule);
    return SearchModule;
}());



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL21pbmxlbi52YWxpZGF0b3IudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gtZm9ybS5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gtcm91dGluZy5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL2FwcC9zZWFyY2gvc2VhcmNoLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpRDtBQUM0QjtBQVM3RTtJQUVJO1FBQ0ksRUFBRTtJQUNOLENBQUM7d0JBSlEsZUFBZTtJQUt4QixrQ0FBUSxHQUFSLFVBQVMsT0FBd0I7UUFDN0IsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtlQUN0RCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztJQUN4QyxDQUFDOztJQVBRO1FBQVIsMkRBQUssRUFBRTs7bURBQWdCO0lBRGYsZUFBZTtRQVAzQiwrREFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFVBQVU7WUFDcEIsU0FBUyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLDREQUFhLEVBQUUsV0FBVyxFQUFFLGlCQUFlO29CQUNwRCxLQUFLLEVBQUUsSUFBSTtpQkFDZCxDQUFDO1NBQ0wsQ0FBQzs7T0FDVyxlQUFlLENBUzNCO0lBQUQsc0JBQUM7Q0FBQTtBQVQyQjs7Ozs7Ozs7O0FDVjVCO0FBQUE7QUFBQTtBQUE4RTtBQXFCOUU7SUFuQkE7UUFvQkksbUJBQWMsR0FBVyxFQUFFLENBQUM7UUFDbEIsV0FBTSxHQUF5QixJQUFJLDBEQUFZLEVBQUUsQ0FBQztJQWVoRSxDQUFDO0lBWkcsc0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU87SUFFdEMsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNsQyxJQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDMUM7SUFDTCxDQUFDO0lBYlM7UUFBVCw0REFBTSxFQUFFO2tDQUFTLDBEQUFZO3VEQUE4QjtJQUNuRDtRQUFSLDJEQUFLLEVBQUU7O3dEQUFpQjtJQUhoQixtQkFBbUI7UUFuQi9CLCtEQUFTLENBQUU7WUFDUixRQUFRLEVBQUUsWUFBWTtZQUV0QixRQUFRLEVBQUUscWVBYVQ7U0FDSixDQUFDO09BRVcsbUJBQW1CLENBaUIvQjtJQUFELDBCQUFDO0NBQUE7QUFqQitCOzs7Ozs7Ozs7QUNyQmhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUU4QjtBQUVsQjtBQUVyRCxJQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlFQUFlLEVBQUU7Q0FDM0MsQ0FBQztBQU1GO0lBQUE7SUFBbUMsQ0FBQztJQUF2QixtQkFBbUI7UUFKL0IsOERBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxPQUFPLEVBQUUsQ0FBQyxvRkFBd0IsQ0FBQztTQUN0QyxDQUFDO09BQ1csbUJBQW1CLENBQUk7SUFBRCwwQkFBQztDQUFBO0FBQUo7Ozs7Ozs7O0FDZGhDLGc0QkFBZzRCLGltQzs7Ozs7Ozs7QUNBaDRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF3RTtBQUVwQjtBQUVVO0FBQ2pCO0FBQ2dDO0FBRWxDO0FBQ2M7QUFRekQ7SUFJSSx5QkFBb0IsUUFBeUIsRUFDakMsS0FBc0I7UUFEZCxhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUNqQyxVQUFLLEdBQUwsS0FBSyxDQUFpQjtRQUM5QixxREFBcUQ7SUFDekQsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDSSxtQ0FBbUM7UUFDbkMsbUNBQW1DO1FBQ25DLG1DQUFtQztRQUNuQyx3QkFBd0I7UUFDeEIsK0RBQStEO1FBQy9ELCtEQUErRDtRQUMvRCwwQkFBMEI7UUFDMUIsMEJBQTBCO1FBQzFCLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLEtBQUssSUFBSyxZQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBdkIsQ0FBdUIsQ0FBQzthQUNoRCxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ1osSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ2YsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUNYLHdEQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsNERBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZGO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCO1FBQ0ksSUFBTSxVQUFVLEdBQWtCLHdFQUFlLEVBQUUsQ0FBQztRQUNwRCxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxJQUFJO1FBQ1Ysa0JBQWtCO1FBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksK0VBQWtCLENBQUMsSUFBSSxvRUFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFDRCxxQ0FBVyxHQUFYLFVBQVksQ0FBQztRQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDZixtRUFBcUIsQ0FBQyxDQUFDLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBR0gscUNBQVcsR0FBWCxVQUFZLENBQVM7UUFBckIsaUJBb0JDO1FBbkJHLDBFQUEwRTtRQUMxRSxrQkFBa0I7UUFDbEIsa0RBQWtEO1FBQ2xELG1CQUFtQjtRQUNuQiwwQ0FBMEM7UUFDMUMscUJBQXFCO1FBQ3JCLGlCQUFpQjtRQUNqQixpQ0FBaUM7UUFDakMsMkNBQTJDO1FBQzNDLHFCQUFxQjtRQUNyQiw2QkFBNkI7UUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBSztZQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRCxLQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUN4QixDQUFDLEVBQUUsVUFBQyxDQUFDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQyx3REFBVSxDQUFDLEVBQUMsSUFBSSxFQUFFLHNCQUFzQixFQUFFLFFBQVEsRUFBRSw0REFBYyxDQUFDLEtBQUssRUFBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELGtDQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUV4QyxDQUFDO0lBQ0QsZ0NBQU0sR0FBTixVQUFPLEdBQUc7UUFBVixpQkFVQztRQVRHLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUMxQyxJQUFJLE9BQU8sR0FBRyxPQUFPLEdBQUcsUUFBUSxDQUFDO1FBRWpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUMvQixVQUFVLENBQUM7WUFDUCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5QixXQUFXLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsbUNBQVMsR0FBVCxVQUFVLElBQUk7UUFDVixLQUFLLENBQUMsb0NBQW9DLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Z0JBL0U2Qix5RUFBZTtnQkFDMUIsaURBQUs7O0lBSEU7UUFBekIsK0RBQVMsQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDO2tDQUFVLHdEQUFVO21EQUFBO0lBRnBDLGVBQWU7UUFOM0IsK0RBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLG1FQUFzQztTQUd6QyxDQUFDO3lDQUtnQyx5RUFBZTtZQUMxQixpREFBSztPQUxmLGVBQWUsQ0FxRjNCO0lBQUQsc0JBQUM7Q0FBQTtBQXJGMkI7Ozs7Ozs7OztBQ2pCNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyRDtBQUNZO0FBQ0Y7QUFFUDtBQUNUO0FBRVM7QUFDRDtBQWtCN0Q7SUFBQTtJQUE0QixDQUFDO0lBQWhCLFlBQVk7UUFoQnhCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0ZBQXdCO2dCQUN4QiwwRUFBbUI7Z0JBQ25CLGtGQUF1QjthQUMxQjtZQUNELFlBQVksRUFBRTtnQkFDVixpRUFBZTtnQkFDZiwwRUFBbUI7Z0JBQ25CLHdFQUFlO2FBQ2xCO1lBQ0QsK0JBQStCO1lBQy9CLE9BQU8sRUFBRTtnQkFDTCw4REFBZ0I7YUFDbkI7U0FDSixDQUFDO09BQ1csWUFBWSxDQUFJO0lBQUQsbUJBQUM7Q0FBQTtBQUFKIiwiZmlsZSI6IjAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgTkdfVkFMSURBVE9SUywgVmFsaWRhdG9yIH0gZnJvbSAgIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuQERpcmVjdGl2ZSh7XG4gICAgc2VsZWN0b3I6IFwiW21pbmxlbl1cIixcbiAgICBwcm92aWRlcnM6IFt7XG4gICAgICAgIHByb3ZpZGU6IE5HX1ZBTElEQVRPUlMsIHVzZUV4aXN0aW5nOiBNaW5MZW5EaXJlY3RpdmUsXG4gICAgICAgIG11bHRpOiB0cnVlXG4gICAgfV1cbn0pXG5leHBvcnQgY2xhc3MgTWluTGVuRGlyZWN0aXZlIGltcGxlbWVudHMgVmFsaWRhdG9yIHtcbiAgICBASW5wdXQoKSBtaW5sZW46IHN0cmluZztcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgLy9cbiAgICB9XG4gICAgdmFsaWRhdGUoY29udHJvbDogQWJzdHJhY3RDb250cm9sKTogeyBba2V5OiBzdHJpbmddOiBhbnkgfSB7XG4gICAgICAgIHJldHVybiAhY29udHJvbC52YWx1ZSB8fCBjb250cm9sLnZhbHVlLmxlbmd0aCA+PSAodGhpcy5taW5sZW5cbiAgICAgICAgICAgIHx8IDIpID8gbnVsbCA6IHsgbWlubGVuOiB0cnVlIH07XG4gICAgfVxufSIsImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBDb21wb25lbnQgKHtcbiAgICBzZWxlY3RvcjogXCJTZWFyY2hGb3JtXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZTogYFxuICBcbiAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVwicm93XCI+IFxuICAgIDxUZXh0RmllbGQgI3RleHRvPVwibmdNb2RlbFwiIFsobmdNb2RlbCldPVwidGV4dEZpZWxkVmFsdWVcIlxuICAgIGhpbnQ9XCJJbmdyZXNhciB0ZXh0by4uLlwiIHJlcXVpcmVkIG1pbmxlbj1cIjRcIj5cbiAgICA8L1RleHRGaWVsZD5cbiAgICA8TGFiZWwgKm5nSWY9XCJ0ZXh0by5oYXNFcnJvcigncmVxdWlyZWQnKVwiIHRleHQ9XCIqXCI+PC9MYWJlbD5cbiAgICA8TGFiZWwgKm5nSWY9XCIhdGV4dG8uaGFzRXJyb3IoJ3JlcXVpcmVkJylcbiAgICAmJiB0ZXh0by5oYXNFcnJvcignbWlubGVuJylcIiB0ZXh0PVwiNCtcIj5cbiAgICA8L0xhYmVsPlxuICAgIDwvRmxleGJveExheW91dD5cbiAgICA8QnV0dG9uIHRleHQ9XCJCdXNjYXIhXCIgKHRhcCk9XCJvbkJ1dHRvblRhcCgpXCIgKm5nSWY9XCJ0ZXh0by52YWxpZFwiPjwvQnV0dG9uPiBcblxuICAgIGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBTZWFyY2hGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICB0ZXh0RmllbGRWYWx1ZTogc3RyaW5nID0gXCJcIjtcbiAgICBAT3V0cHV0KCkgc2VhcmNoOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBASW5wdXQoKSBpbmljaWFsOiBzdHJpbmc7XG5cbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy50ZXh0RmllbGRWYWx1ZSA9IHRoaXMuaW5pY2lhbFxuICAgICAgICBcbiAgICB9XG5cbiAgICBvbkJ1dHRvblRhcCgpOiB2b2lkIHtcbiAgICAgICAgY29uc29sZS5sb2cgKHRoaXMudGV4dEZpZWxkVmFsdWUpO1xuICAgICAgICBpZih0aGlzLnRleHRGaWVsZFZhbHVlLmxlbmd0aD4yKSB7IFxuICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdCAodGhpcy50ZXh0RmllbGRWYWx1ZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbn0iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbmltcG9ydCB7IFNlYXJjaENvbXBvbmVudCB9IGZyb20gXCIuL3NlYXJjaC5jb21wb25lbnRcIjtcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IFNlYXJjaENvbXBvbmVudCB9XG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXG4gICAgZXhwb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoUm91dGluZ01vZHVsZSB7IH1cbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXG4gICAgPCEtLSBcXG4gICAgVXNlIHRoZSBOYXZpZ2F0aW9uQnV0dG9uIGFzIGEgc2lkZS1kcmF3ZXIgYnV0dG9uIGluIEFuZHJvaWRcXG4gICAgYmVjYXVzZSBBY3Rpb25JdGVtcyBhcmUgc2hvd24gb24gdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIEFjdGlvbkJhclxcbiAgICAtLT5cXG4gICAgPE5hdmlnYXRpb25CdXR0b24gaW9zOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgaWNvbj1cXFwicmVzOi8vbWVudVxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiPjwvTmF2aWdhdGlvbkJ1dHRvbj5cXG4gICAgPCEtLSBcXG4gICAgVXNlIHRoZSBBY3Rpb25JdGVtIGZvciBJT1Mgd2l0aCBwb3NpdGlvbiBzZXQgdG8gbGVmdC4gVXNpbmcgdGhlXFxuICAgIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gaU9TIGlzIG5vdCBwb3NzaWJsZSxcXG4gICAgYmVjYXVzZSBpdHMgZnVuY3Rpb24gaXMgdG8gYWx3YXlzIG5hdmlnYXRlIGJhY2sgaW4gdGhlIGFwcGxpY2F0aW9uLlxcbiAgICAtLT5cXG4gICAgPEFjdGlvbkl0ZW0gaWNvbj1cXFwicmVzOi8vbmF2aWdhdGlvbi9tZW51XFxcIiBhbmRyb2lkOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiIGlvcy5wb3NpdGlvbj1cXFwibGVmdFxcXCI+XFxuICAgIDwvQWN0aW9uSXRlbT5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJhY3Rpb24tYmFyLXRpdGxlXFxcIiB0ZXh0PVxcXCJTZWFyY2hcXFwiPjwvTGFiZWw+XFxuPC9BY3Rpb25CYXI+XFxuXFxuXFxuPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJwYWdlIHBhZ2UtY29udGVudFxcXCIgI2xheW91dD5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlLWljb24gZmFcXFwiIHRleHQ9XFxcIiYjeGYwMDI7XFxcIj48L0xhYmVsPlxcbiAgICA8U2VhcmNoRm9ybSAoc2VhcmNoKT1cXFwiYnVzY2FyQWhvcmEoJGV2ZW50KVxcXCIgW2luaWNpYWxdPVxcXCInaG9sYSdcXFwiPjwvU2VhcmNoRm9ybT5cXG4gICAgICAgIDwhLS0gPFB1bGxUb1JlZnJlc2ggKHJlZnJlc2gpPVxcXCJvblB1bGwoJGV2ZW50KVxcXCI+IC0tPlxcbiAgICAgICAgPExpc3RWaWV3IGNsYXNzPVxcXCJsaXN0LWdyb3VwXFxcIiBbaXRlbXNdPVxcXCJ0aGlzLnJlc3VsdGFkb3NcXFwiIChpdGVtVGFwKT1cXFwib25JdGVtVGFwKCRldmVudClcXFwiIHN0eWxlPVxcXCJoZWlnaHQ6IDEyNTBweFxcXCI+XFxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlIGxldC14PVxcXCJpdGVtXFxcIj5cXG4gICAgICAgICAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVxcXCJyb3dcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwLWl0ZW1cXFwiPlxcbiAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJyZXM6Ly9pY29uXFxcIiBjbGFzcz1cXFwidGh1bWIgaW1nLWNpcmNsZVxcXCI+PC9JbWFnZT5cXG4gICAgICAgICAgICAgICAgPExhYmVsIFt0ZXh0XT1cXFwieFxcXCIgY2xhc3M9XFxcImxpc3QtZ3JvdXAtaXRlbS1oZWFkaW5nXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBzdHlsZT1cXFwid2lkdGg6IDEwMCVcXFwiPjwvTGFiZWw+ICAgICAgICAgICAgICAgIFxcbiAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkJvcnJhclxcXCIgY2xhc3M9XFxcImJ0blxcXCIgKHRhcCk9XFxcIm9uRGVsZXRlKHgpXFxcIiBzdHlsZT1cXFwid2lkdGg6IDI1JVxcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiRGV0YWxsZVxcXCIgY2xhc3M9XFxcImJ0blxcXCIgKHRhcCk9XFxcIm9uRGV0YWxsZSh4KVxcXCIgc3R5bGU9XFxcIndpZHRoOiAyNSVcXFwiID48L0J1dHRvbj5cXG4gICAgICAgICAgICAgICAgPEltYWdlIHNyYz1cXFwicmVzOi8vaWNvblxcXCIgY2xhc3M9XFxcInRodW1iIGltZy1jaXJjbGVcXFwiIChsb25nUHJlc3MpPVxcXCJvbkxvbmdQcmVzcyh4KVxcXCIgc3R5bGU9XFxcIndpZHRoOiAyNSVcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcbiAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XFxuICAgICAgICA8L0xpc3RWaWV3PlxcbiAgICAgICAgICAgICAgICA8IS0tIDwvUHVsbFRvUmVmcmVzaD4gLS0+XFxuPC9TdGFja0xheW91dD5cXG5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZixPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyXCI7XG5pbXBvcnQgKiBhcyBhcHAgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIjtcbmltcG9ydCB7Q29sb3IsIFZpZXd9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2NvcmUvdmlldy92aWV3XCJcbmltcG9ydCB7IE5vdGljaWFzU2VydmljZSB9IGZyb20gXCIuLi9kb21haW4vbm90aWNpYXMuc2VydmljZXNcIjtcbmltcG9ydCAqIGFzIFRvYXN0IGZyb20gXCJuYXRpdmVzY3JpcHQtdG9hc3RzXCI7XG5pbXBvcnQgeyBOb3RpY2lhLCBOdWV2YU5vdGljaWFBY3Rpb24gfSBmcm9tIFwiLi4vZG9tYWluL25vdGljaWFzLXN0YXRlLm1vZGVsXCI7XG5pbXBvcnQgeyBBcHBTdGF0ZSB9IGZyb20gXCIuLi9hcHAubW9kdWxlXCI7XG5pbXBvcnQgeyBTdG9yZSwgU3RhdGUgfSBmcm9tIFwiQG5ncngvc3RvcmVcIjtcbmltcG9ydCAqIGFzIFNvY2lhbFNoYXJlIGZyb20gXCJuYXRpdmVzY3JpcHQtc29jaWFsLXNoYXJlXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIlNlYXJjaFwiLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vc2VhcmNoLmNvbXBvbmVudC5odG1sXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZFxuICAgIC8vcHJvdmlkZXJzOiBbTm90aWNpYXNTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHJlc3VsdGFkb3M6IEFycmF5PHN0cmluZz47XG4gICAgQFZpZXdDaGlsZChcImxheW91dFwiLG51bGwpIGxheW91dCA6IEVsZW1lbnRSZWZcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbm90aWNpYXM6IE5vdGljaWFzU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBzdG9yZTogU3RvcmU8QXBwU3RhdGU+KSB7XG4gICAgICAgIC8vIFVzZSB0aGUgY29tcG9uZW50IGNvbnN0cnVjdG9yIHRvIGluamVjdCBwcm92aWRlcnMuXG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIC8vIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGEgMVwiKTtcbiAgICAgICAgLy8gdGhpcy5ub3RpY2lhcy5hZ3JlZ2FyKFwiaG9sYSAyXCIpO1xuICAgICAgICAvLyB0aGlzLm5vdGljaWFzLmFncmVnYXIoXCJob2xhIDNcIik7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwiYXNmYWZcIik7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHtub21icmU6IHtub21icmU6IHtub21icmU6IHtub21icmU6IFwicGVwZVwifX19fSk7XG4gICAgICAgIC8vIGNvbnNvbGUuZGlyKHtub21icmU6IHtub21icmU6IHtub21icmU6IHtub21icmU6IFwicGVwZVwifX19fSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFsxLCAyLCAzXSk7XG4gICAgICAgIC8vIGNvbnNvbGUuZGlyKFs0LCA1LCA2XSk7XG4gICAgICAgIC8vIEluaXQgeW91ciBjb21wb25lbnQgcHJvcGVydGllcyBoZXJlLlxuICAgICAgICB0aGlzLnN0b3JlLnNlbGVjdCgoc3RhdGUpID0+IHN0YXRlLm5vdGljaWFzLnN1Z2VyaWRhKVxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGYgPSBkYXRhO1xuICAgICAgICAgICAgICAgIGlmIChmICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgVG9hc3Quc2hvdyh7IHRleHQ6IFwiU3VnZXJpbW9zIGxlZXI6IFwiICsgZi50aXR1bG8sIGR1cmF0aW9uOiBUb2FzdC5EVVJBVElPTi5TSE9SVCB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBvbkRyYXdlckJ1dHRvblRhcCgpOiB2b2lkIHtcbiAgICAgICAgY29uc3Qgc2lkZURyYXdlciA9IDxSYWRTaWRlRHJhd2VyPmFwcC5nZXRSb290VmlldygpO1xuICAgICAgICBzaWRlRHJhd2VyLnNob3dEcmF3ZXIoKTtcbiAgICB9XG5cbiAgICBvbkl0ZW1UYXAoYXJncyk6IHZvaWQge1xuICAgICAgICAvLyBjb25zb2xlLmRpcih4KTtcbiAgICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgTnVldmFOb3RpY2lhQWN0aW9uKG5ldyBOb3RpY2lhKGFyZ3Mudmlldy5iaW5kaW5nQ29udGV4dCkpKTtcbiAgICB9XG4gICAgb25Mb25nUHJlc3Mocykge1xuICAgICAgICBjb25zb2xlLmxvZyhzKTtcbiAgICAgICAgU29jaWFsU2hhcmUuc2hhcmVUZXh0KHMsIFwiQXN1bnRvOiBDb21wYXJ0aWRvIGRlc2RlIGVsIGN1cnNvIVwiKTtcbiAgICAgIH1cbiAgICBcblxuICAgIGJ1c2NhckFob3JhKHM6IHN0cmluZykge1xuICAgICAgICAvLyB0aGlzLnJlc3VsdGFkb3MgPSB0aGlzLm5vdGljaWFzLmJ1c2NhcigpLmZpbHRlcigoeCk9PiB4LmluZGV4T2Yocyk+PTApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhzKTtcbiAgICAgICAgLy8gY29uc3QgbGF5b3V0ID0gPFZpZXc+dGhpcy5sYXlvdXQubmF0aXZlRWxlbWVudDtcbiAgICAgICAgLy8gbGF5b3V0LmFuaW1hdGUoe1xuICAgICAgICAvLyAgICAgYmFja2dyb3VuZENvbG9yOiBuZXcgQ29sb3IoXCJibHVlXCIpLFxuICAgICAgICAvLyAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgLy8gICAgIGRlbGF5OiAxNTBcbiAgICAgICAgLy8gfSkudGhlbigoKSA9PiBsYXlvdXQuYW5pbWF0ZSh7XG4gICAgICAgIC8vICAgICBiYWNrZ3JvdW5kQ29sb3I6IG5ldyBDb2xvcihcIndoaXRlXCIpLFxuICAgICAgICAvLyAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgLy8gICAgIGRlbGF5OiAxNTAgICAgICAgIH0pKTtcbiAgICAgICAgY29uc29sZS5kaXIoXCJidXNjYXJBaG9yYVwiK3MpO1xuICAgICAgICB0aGlzLm5vdGljaWFzLmJ1c2NhcihzKS50aGVuKChyOmFueSkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJyZXN1bHRhZG9zIGJ1c2NhckFob3JhIFwiKyBKU09OLnN0cmluZ2lmeShyKSk7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdGFkb3MgPSByO1xuICAgICAgICB9LCAoZSk9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yIGJ1c2NhckFob3JhIFwiK2UpO1xuICAgICAgICAgICAgVG9hc3Quc2hvdyh7dGV4dDogXCJFcnJvciBlbiBsYSBidXNxdWVkYVwiLCBkdXJhdGlvbjogVG9hc3QuRFVSQVRJT04uU0hPUlR9KVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgb25EZWxldGUoaXRlbSk6IHZvaWQge1xuICAgICAgICB0aGlzLnJlc3VsdGFkb3Muc3BsaWNlKGl0ZW0sIDEpO1xuICAgICAgICBhbGVydCgnU2UgZWxpbWlubyBlbCBpdGVtICcgKyBpdGVtKTtcblxuICAgIH1cbiAgICBvblB1bGwoYXJnKSB7XG4gICAgICAgIGxldCBjb250YWRvciA9IHRoaXMucmVzdWx0YWRvcy5sZW5ndGggKyAxO1xuICAgICAgICBsZXQgc2FsdWRvcyA9IFwiSG9sYSBcIiArIGNvbnRhZG9yO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKGFyZyk7XG4gICAgICAgIGNvbnN0IHB1bGxSZWZyZXNoID0gYXJnLm9iamVjdDtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdGFkb3MucHVzaChzYWx1ZG9zKTtcbiAgICAgICAgICAgIHB1bGxSZWZyZXNoLnJlZnJlc2hpbmcgPSBmYWxzZTtcbiAgICAgICAgfSwgMTAwMCk7XG4gICAgfVxuXG4gICAgb25EZXRhbGxlKGl0ZW0pOiB2b2lkIHtcbiAgICAgICAgYWxlcnQoJ01vc3RyYXIgbG9zIGRldGFsbGVzIGRlbCBlbGVtZW50byAnICsgaXRlbSk7XG4gICAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuXG5pbXBvcnQgeyBTZWFyY2hSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vc2VhcmNoLXJvdXRpbmcubW9kdWxlXCI7XG5pbXBvcnQgeyBTZWFyY2hDb21wb25lbnQgfSBmcm9tIFwiLi9zZWFyY2guY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOb3RpY2lhc1NlcnZpY2UgfSBmcm9tIFwiLi4vZG9tYWluL25vdGljaWFzLnNlcnZpY2VzXCI7XG5pbXBvcnQgeyBTZWFyY2hGb3JtQ29tcG9uZW50IH0gZnJvbSBcIi4vc2VhcmNoLWZvcm0uY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNaW5MZW5EaXJlY3RpdmUgfSBmcm9tIFwiLi4vc2VhcmNoL21pbmxlbi52YWxpZGF0b3JcIjtcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcbiAgICAgICAgU2VhcmNoUm91dGluZ01vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBTZWFyY2hDb21wb25lbnQsXG4gICAgICAgIFNlYXJjaEZvcm1Db21wb25lbnQsXG4gICAgICAgIE1pbkxlbkRpcmVjdGl2ZVxuICAgIF0sXG4gICAgLy9wcm92aWRlcnM6IFtOb3RpY2lhc1NlcnZpY2VdLFxuICAgIHNjaGVtYXM6IFtcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoTW9kdWxlIHsgfVxuIl0sInNvdXJjZVJvb3QiOiIifQ==