(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[1],{

/***/ "./app/carrito/carrito-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarritoRoutingModule", function() { return CarritoRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _carrito_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/carrito/carrito.component.ts");



var routes = [
    { path: "", component: _carrito_component__WEBPACK_IMPORTED_MODULE_2__["CarritoComponent"] },
    { path: "compras", loadChildren: function () { return __webpack_require__.e(/* import() */ 3).then(__webpack_require__.bind(null, "./app/carrito/compras/compras.module.ts")).then(function (m) { return m.ComprasModule; }); } },
    { path: "deseos", loadChildren: function () { return __webpack_require__.e(/* import() */ 4).then(__webpack_require__.bind(null, "./app/carrito/deseos/deseos.module.ts")).then(function (m) { return m.DeseosModule; }); } }
];
var CarritoRoutingModule = /** @class */ (function () {
    function CarritoRoutingModule() {
    }
    CarritoRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], CarritoRoutingModule);
    return CarritoRoutingModule;
}());



/***/ }),

/***/ "./app/carrito/carrito.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar class=\"action-bar\">\n    <!-- \n        Use the NavigationButton as a side-drawer button in Android\n        because ActionItems are shown on the right side of the ActionBar\n        -->\n    <NavigationButton ios:visibility=\"collapsed\" icon=\"res://menu\" (tap)=\"onDrawerButtonTap()\"></NavigationButton>\n    <!-- \n        Use the ActionItem for IOS with position set to left. Using the\n        NavigationButton as a side-drawer button in iOS is not possible,\n        because its function is to always navigate back in the application.\n        -->\n    <ActionItem icon=\"res://navigation/menu\" android:visibility=\"collapsed\" (tap)=\"onDrawerButtonTap()\" ios.position=\"left\">\n    </ActionItem>\n    <Label class=\"action-bar-title\" text=\"Carrito\"></Label>\n</ActionBar>\n\n<GridLayout class=\"page page-content\">\n    <Label class=\"page-icon fa\" text=\"&#xf07a;\"></Label>\n    <StackLayout>\n        <Image src=\"res://cart\" stretch=\"aspectFill\" class=\"fas t-36\"></Image>\n        <Button text=\"Compras\" class=\"buttontest\" (tap)=\"onNavItemTap('/carrito/compras')\"></Button>\n        <Image src=\"res://piggybank\" stretch=\"aspectFill\" class=\"fas t-36\"></Image>\n        <Button text=\"Deseos\" class=\"buttontest\" (tap)=\"onNavItemTap('/carrito/deseos')\"></Button>\n    </StackLayout>\n    <Label class=\"page-placeholder\" text=\"<!-- Carrito -->\"></Label>\n</GridLayout>"

/***/ }),

/***/ "./app/carrito/carrito.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarritoComponent", function() { return CarritoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_2__);



var CarritoComponent = /** @class */ (function () {
    function CarritoComponent(routerExtensions) {
        this.routerExtensions = routerExtensions;
        console.log("tests");
        // Use the component constructor to inject providers.
    }
    CarritoComponent.prototype.ngOnInit = function () {
        // Init your component properties here.
    };
    CarritoComponent.prototype.onDrawerButtonTap = function () {
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__["getRootView"]();
        sideDrawer.showDrawer();
    };
    CarritoComponent.prototype.onNavItemTap = function (navItemRoute) {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__["getRootView"]();
        sideDrawer.closeDrawer();
    };
    CarritoComponent.ctorParameters = function () { return [
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterExtensions"] }
    ]; };
    CarritoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Carrito",
            template: __webpack_require__("./app/carrito/carrito.component.html")
        }),
        __metadata("design:paramtypes", [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterExtensions"]])
    ], CarritoComponent);
    return CarritoComponent;
}());



/***/ }),

/***/ "./app/carrito/carrito.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarritoModule", function() { return CarritoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/common.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _carrito_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/carrito/carrito-routing.module.ts");
/* harmony import */ var _carrito_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/carrito/carrito.component.ts");
/* harmony import */ var _domain_carrito_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/domain/carrito.services.ts");





var CarritoModule = /** @class */ (function () {
    function CarritoModule() {
    }
    CarritoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _carrito_routing_module__WEBPACK_IMPORTED_MODULE_2__["CarritoRoutingModule"]
            ],
            declarations: [
                _carrito_component__WEBPACK_IMPORTED_MODULE_3__["CarritoComponent"]
            ],
            providers: [_domain_carrito_services__WEBPACK_IMPORTED_MODULE_4__["CarritoService"]],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], CarritoModule);
    return CarritoModule;
}());



/***/ }),

/***/ "./app/domain/carrito.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarritoService", function() { return CarritoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");

var CarritoService = /** @class */ (function () {
    function CarritoService() {
        this.compras = [];
        this.deseos = [];
    }
    // tslint:disable-next-line: ban-types
    CarritoService.prototype.agregaCompras = function (s) {
        this.compras.push(s);
    };
    CarritoService.prototype.buscarCompras = function () {
        return this.compras;
    };
    CarritoService.prototype.agregaDeseos = function (s) {
        this.deseos.push(s);
    };
    CarritoService.prototype.buscarDeseos = function () {
        return this.deseos;
    };
    CarritoService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], CarritoService);
    return CarritoService;
}());



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvY2Fycml0by9jYXJyaXRvLXJvdXRpbmcubW9kdWxlLnRzIiwid2VicGFjazovLy8uL2FwcC9jYXJyaXRvL2NhcnJpdG8uY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwL2NhcnJpdG8vY2Fycml0by5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2NhcnJpdG8vY2Fycml0by5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2RvbWFpbi9jYXJyaXRvLnNlcnZpY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUU4QjtBQUVoQjtBQUV2RCxJQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLG1FQUFnQixFQUFFO0lBQ3pDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsY0FBTSw4SEFBOEMsQ0FBQyxJQUFJLENBQUMsV0FBQyxJQUFJLFFBQUMsQ0FBQyxhQUFhLEVBQWYsQ0FBZSxDQUFDLEVBQXpFLENBQXlFLEVBQUU7SUFDbEgsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxjQUFNLDRIQUE0QyxDQUFDLElBQUksQ0FBQyxXQUFDLElBQUksUUFBQyxDQUFDLFlBQVksRUFBZCxDQUFjLENBQUMsRUFBdEUsQ0FBc0UsRUFBRztDQUNsSCxDQUFDO0FBTUY7SUFBQTtJQUFvQyxDQUFDO0lBQXhCLG9CQUFvQjtRQUpoQyw4REFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0ZBQXdCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxvQkFBb0IsQ0FBSTtJQUFELDJCQUFDO0NBQUE7QUFBSjs7Ozs7Ozs7QUNoQmpDLGs1QkFBazVCLG1oQjs7Ozs7Ozs7QUNBbDVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtEO0FBRUU7QUFDVztBQU0vRDtJQUVJLDBCQUFvQixnQkFBa0M7UUFBbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLHFEQUFxRDtJQUN6RCxDQUFDO0lBRUQsbUNBQVEsR0FBUjtRQUNJLHVDQUF1QztJQUMzQyxDQUFDO0lBRUQsNENBQWlCLEdBQWpCO1FBQ0ksSUFBTSxVQUFVLEdBQWtCLHdFQUFlLEVBQUUsQ0FBQztRQUNwRCxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNELHVDQUFZLEdBQVosVUFBYSxZQUFvQjtRQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDM0MsVUFBVSxFQUFFO2dCQUNSLElBQUksRUFBRSxNQUFNO2FBQ2Y7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFNLFVBQVUsR0FBa0Isd0VBQWUsRUFBRSxDQUFDO1FBQ3BELFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM3QixDQUFDOztnQkF0QnFDLDRFQUFnQjs7SUFGN0MsZ0JBQWdCO1FBSjVCLCtEQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsU0FBUztZQUNuQixxRUFBdUM7U0FDMUMsQ0FBQzt5Q0FHd0MsNEVBQWdCO09BRjdDLGdCQUFnQixDQXlCNUI7SUFBRCx1QkFBQztDQUFBO0FBekI0Qjs7Ozs7Ozs7O0FDVDdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkQ7QUFDWTtBQUVQO0FBQ1Q7QUFDSztBQWU1RDtJQUFBO0lBQTZCLENBQUM7SUFBakIsYUFBYTtRQWJ6Qiw4REFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLG9GQUF3QjtnQkFDeEIsNEVBQW9CO2FBQ3ZCO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLG1FQUFnQjthQUNuQjtZQUNELFNBQVMsRUFBRSxDQUFDLHVFQUFjLENBQUM7WUFDM0IsT0FBTyxFQUFFO2dCQUNMLDhEQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxhQUFhLENBQUk7SUFBRCxvQkFBQztDQUFBO0FBQUo7Ozs7Ozs7OztBQ3BCMUI7QUFBQTtBQUFBO0FBQTJDO0FBRzNDO0lBREE7UUFFWSxZQUFPLEdBQWtCLEVBQUUsQ0FBQztRQUM1QixXQUFNLEdBQWtCLEVBQUUsQ0FBQztJQWV2QyxDQUFDO0lBZEcsc0NBQXNDO0lBRXRDLHNDQUFhLEdBQWIsVUFBYyxDQUFTO1FBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFDRCxzQ0FBYSxHQUFiO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxxQ0FBWSxHQUFaLFVBQWEsQ0FBUztRQUNsQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBQ0QscUNBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBaEJRLGNBQWM7UUFEMUIsZ0VBQVUsRUFBRTtPQUNBLGNBQWMsQ0FpQjFCO0lBQUQscUJBQUM7Q0FBQTtBQWpCMEIiLCJmaWxlIjoiMS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuaW1wb3J0IHsgQ2Fycml0b0NvbXBvbmVudCB9IGZyb20gXCIuL2NhcnJpdG8uY29tcG9uZW50XCI7XG5cbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICAgIHsgcGF0aDogXCJcIiwgY29tcG9uZW50OiBDYXJyaXRvQ29tcG9uZW50IH0sXG4gICAgeyBwYXRoOiBcImNvbXByYXNcIiwgbG9hZENoaWxkcmVuOiAoKSA9PiBpbXBvcnQoYH4vYXBwL2NhcnJpdG8vY29tcHJhcy9jb21wcmFzLm1vZHVsZWApLnRoZW4obSA9PiBtLkNvbXByYXNNb2R1bGUpIH0sXG4gICAgeyBwYXRoOiBcImRlc2Vvc1wiLCBsb2FkQ2hpbGRyZW46ICgpID0+IGltcG9ydChgfi9hcHAvY2Fycml0by9kZXNlb3MvZGVzZW9zLm1vZHVsZWApLnRoZW4obSA9PiBtLkRlc2Vvc01vZHVsZSkgIH0gXG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXG4gICAgZXhwb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgQ2Fycml0b1JvdXRpbmdNb2R1bGUgeyB9XG4iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPEFjdGlvbkJhciBjbGFzcz1cXFwiYWN0aW9uLWJhclxcXCI+XFxuICAgIDwhLS0gXFxuICAgICAgICBVc2UgdGhlIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gQW5kcm9pZFxcbiAgICAgICAgYmVjYXVzZSBBY3Rpb25JdGVtcyBhcmUgc2hvd24gb24gdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIEFjdGlvbkJhclxcbiAgICAgICAgLS0+XFxuICAgIDxOYXZpZ2F0aW9uQnV0dG9uIGlvczp2aXNpYmlsaXR5PVxcXCJjb2xsYXBzZWRcXFwiIGljb249XFxcInJlczovL21lbnVcXFwiICh0YXApPVxcXCJvbkRyYXdlckJ1dHRvblRhcCgpXFxcIj48L05hdmlnYXRpb25CdXR0b24+XFxuICAgIDwhLS0gXFxuICAgICAgICBVc2UgdGhlIEFjdGlvbkl0ZW0gZm9yIElPUyB3aXRoIHBvc2l0aW9uIHNldCB0byBsZWZ0LiBVc2luZyB0aGVcXG4gICAgICAgIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gaU9TIGlzIG5vdCBwb3NzaWJsZSxcXG4gICAgICAgIGJlY2F1c2UgaXRzIGZ1bmN0aW9uIGlzIHRvIGFsd2F5cyBuYXZpZ2F0ZSBiYWNrIGluIHRoZSBhcHBsaWNhdGlvbi5cXG4gICAgICAgIC0tPlxcbiAgICA8QWN0aW9uSXRlbSBpY29uPVxcXCJyZXM6Ly9uYXZpZ2F0aW9uL21lbnVcXFwiIGFuZHJvaWQ6dmlzaWJpbGl0eT1cXFwiY29sbGFwc2VkXFxcIiAodGFwKT1cXFwib25EcmF3ZXJCdXR0b25UYXAoKVxcXCIgaW9zLnBvc2l0aW9uPVxcXCJsZWZ0XFxcIj5cXG4gICAgPC9BY3Rpb25JdGVtPlxcbiAgICA8TGFiZWwgY2xhc3M9XFxcImFjdGlvbi1iYXItdGl0bGVcXFwiIHRleHQ9XFxcIkNhcnJpdG9cXFwiPjwvTGFiZWw+XFxuPC9BY3Rpb25CYXI+XFxuXFxuPEdyaWRMYXlvdXQgY2xhc3M9XFxcInBhZ2UgcGFnZS1jb250ZW50XFxcIj5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlLWljb24gZmFcXFwiIHRleHQ9XFxcIiYjeGYwN2E7XFxcIj48L0xhYmVsPlxcbiAgICA8U3RhY2tMYXlvdXQ+XFxuICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJyZXM6Ly9jYXJ0XFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIiBjbGFzcz1cXFwiZmFzIHQtMzZcXFwiPjwvSW1hZ2U+XFxuICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkNvbXByYXNcXFwiIGNsYXNzPVxcXCJidXR0b250ZXN0XFxcIiAodGFwKT1cXFwib25OYXZJdGVtVGFwKCcvY2Fycml0by9jb21wcmFzJylcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgPEltYWdlIHNyYz1cXFwicmVzOi8vcGlnZ3liYW5rXFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIiBjbGFzcz1cXFwiZmFzIHQtMzZcXFwiPjwvSW1hZ2U+XFxuICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkRlc2Vvc1xcXCIgY2xhc3M9XFxcImJ1dHRvbnRlc3RcXFwiICh0YXApPVxcXCJvbk5hdkl0ZW1UYXAoJy9jYXJyaXRvL2Rlc2VvcycpXFxcIj48L0J1dHRvbj5cXG4gICAgPC9TdGFja0xheW91dD5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlLXBsYWNlaG9sZGVyXFxcIiB0ZXh0PVxcXCI8IS0tIENhcnJpdG8gLS0+XFxcIj48L0xhYmVsPlxcbjwvR3JpZExheW91dD5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXJcIjtcbmltcG9ydCAqIGFzIGFwcCBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiQ2Fycml0b1wiLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vY2Fycml0by5jb21wb25lbnQuaHRtbFwiXG59KVxuZXhwb3J0IGNsYXNzIENhcnJpdG9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwidGVzdHNcIik7XG4gICAgICAgIC8vIFVzZSB0aGUgY29tcG9uZW50IGNvbnN0cnVjdG9yIHRvIGluamVjdCBwcm92aWRlcnMuXG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIC8vIEluaXQgeW91ciBjb21wb25lbnQgcHJvcGVydGllcyBoZXJlLlxuICAgIH1cblxuICAgIG9uRHJhd2VyQnV0dG9uVGFwKCk6IHZvaWQge1xuICAgICAgICBjb25zdCBzaWRlRHJhd2VyID0gPFJhZFNpZGVEcmF3ZXI+YXBwLmdldFJvb3RWaWV3KCk7XG4gICAgICAgIHNpZGVEcmF3ZXIuc2hvd0RyYXdlcigpO1xuICAgIH1cbiAgICBvbk5hdkl0ZW1UYXAobmF2SXRlbVJvdXRlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtuYXZJdGVtUm91dGVdLCB7XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJmYWRlXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3Qgc2lkZURyYXdlciA9IDxSYWRTaWRlRHJhd2VyPmFwcC5nZXRSb290VmlldygpO1xuICAgICAgICBzaWRlRHJhd2VyLmNsb3NlRHJhd2VyKCk7XG4gICAgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vblwiO1xuXG5pbXBvcnQgeyBDYXJyaXRvUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2NhcnJpdG8tcm91dGluZy5tb2R1bGVcIjtcbmltcG9ydCB7IENhcnJpdG9Db21wb25lbnQgfSBmcm9tIFwiLi9jYXJyaXRvLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ2Fycml0b1NlcnZpY2UgfSBmcm9tIFwiLi4vZG9tYWluL2NhcnJpdG8uc2VydmljZXNcIjtcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcbiAgICAgICAgQ2Fycml0b1JvdXRpbmdNb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBDYXJyaXRvQ29tcG9uZW50XG4gICAgXSxcbiAgICBwcm92aWRlcnM6IFtDYXJyaXRvU2VydmljZV0sXG4gICAgc2NoZW1hczogW1xuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBDYXJyaXRvTW9kdWxlIHsgfVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDYXJyaXRvU2VydmljZSB7XHJcbiAgICBwcml2YXRlIGNvbXByYXM6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuICAgIHByaXZhdGUgZGVzZW9zOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGJhbi10eXBlc1xyXG5cclxuICAgIGFncmVnYUNvbXByYXMoczogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jb21wcmFzLnB1c2gocyk7XHJcbiAgICB9XHJcbiAgICBidXNjYXJDb21wcmFzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbXByYXM7XHJcbiAgICB9XHJcbiAgICBhZ3JlZ2FEZXNlb3Moczogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5kZXNlb3MucHVzaChzKTtcclxuICAgIH1cclxuICAgIGJ1c2NhckRlc2VvcygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kZXNlb3M7XHJcbiAgICB9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==