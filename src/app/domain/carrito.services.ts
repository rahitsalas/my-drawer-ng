import { Injectable } from "@angular/core";

@Injectable()
export class CarritoService {
    private compras: Array<string> = [];
    private deseos: Array<string> = [];
    // tslint:disable-next-line: ban-types

    agregaCompras(s: string) {
        this.compras.push(s);
    }
    buscarCompras() {
        return this.compras;
    }
    agregaDeseos(s: string) {
        this.deseos.push(s);
    }
    buscarDeseos() {
        return this.deseos;
    }
}
