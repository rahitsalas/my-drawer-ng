import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DeseosRoutingModule } from "./deseos-routing.module";
import { DeseosComponent } from "./deseos.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DeseosRoutingModule
    ],
    declarations: [
        DeseosComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DeseosModule { }
