import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { CarritoService } from "../../domain/carrito.services";

@Component({
    selector: "Deseos",
    templateUrl: "./deseos.component.html"
})
export class DeseosComponent implements OnInit {

    constructor(private deseos: CarritoService) {
        console.log("tests2");
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.deseos.agregaDeseos("deseo 1");
        this.deseos.agregaDeseos("deseo 2");
        this.deseos.agregaDeseos("deseo 3");
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
