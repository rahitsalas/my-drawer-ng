import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CarritoComponent } from "./carrito.component";

const routes: Routes = [
    { path: "", component: CarritoComponent },
    { path: "compras", loadChildren: () => import(`~/app/carrito/compras/compras.module`).then(m => m.ComprasModule) },
    { path: "deseos", loadChildren: () => import(`~/app/carrito/deseos/deseos.module`).then(m => m.DeseosModule)  } 
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CarritoRoutingModule { }
