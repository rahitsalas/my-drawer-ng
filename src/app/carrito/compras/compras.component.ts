import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { CarritoService } from "../../domain/carrito.services";
import * as app from "tns-core-modules/application";


@Component({
    selector: "Compras",
    templateUrl: "./compras.component.html"
})
export class ComprasComponent implements OnInit {

    constructor(private compras: CarritoService) {
        console.log("tests1");
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.compras.agregaCompras("compra 1");
        this.compras.agregaCompras("compra 2");
        this.compras.agregaCompras("compra 3");
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
