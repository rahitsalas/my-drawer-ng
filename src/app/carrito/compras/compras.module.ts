import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ComprasRoutingModule } from "./compras-routing.module";
import { ComprasComponent } from "./compras.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ComprasRoutingModule
    ],
    declarations: [
        ComprasComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ComprasModule { }
