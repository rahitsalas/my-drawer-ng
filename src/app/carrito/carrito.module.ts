import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { CarritoRoutingModule } from "./carrito-routing.module";
import { CarritoComponent } from "./carrito.component";
import { CarritoService } from "../domain/carrito.services";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        CarritoRoutingModule
    ],
    declarations: [
        CarritoComponent
    ],
    providers: [CarritoService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class CarritoModule { }
