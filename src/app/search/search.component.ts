import { Component, ElementRef,OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {Color, View} from "tns-core-modules/ui/core/view/view"
import { NoticiasService } from "../domain/noticias.services";
import * as Toast from "nativescript-toasts";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { AppState } from "../app.module";
import { Store, State } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    moduleId: module.id
    //providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout",null) layout : ElementRef

    constructor(private noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // this.noticias.agregar("hola 1");
        // this.noticias.agregar("hola 2");
        // this.noticias.agregar("hola 3");
        // console.log("asfaf");
        // console.log({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.dir({nombre: {nombre: {nombre: {nombre: "pepe"}}}});
        // console.log([1, 2, 3]);
        // console.dir([4, 5, 6]);
        // Init your component properties here.
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({ text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT });
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        // console.dir(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }
    onLongPress(s) {
        console.log(s);
        SocialShare.shareText(s, "Asunto: Compartido desde el curso!");
      }
    

    buscarAhora(s: string) {
        // this.resultados = this.noticias.buscar().filter((x)=> x.indexOf(s)>=0);
        // console.log(s);
        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 300,
        //     delay: 150        }));
        console.dir("buscarAhora"+s);
        this.noticias.buscar(s).then((r:any) => {
            console.log("resultados buscarAhora "+ JSON.stringify(r));
            this.resultados = r;
        }, (e)=> {
            console.log("Error buscarAhora "+e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT})
        });
    }
    onDelete(item): void {
        this.resultados.splice(item, 1);
        alert('Se elimino el item ' + item);

    }
    onPull(arg) {
        let contador = this.resultados.length + 1;
        let saludos = "Hola " + contador;

        console.log(arg);
        const pullRefresh = arg.object;
        setTimeout(() => {
            this.resultados.push(saludos);
            pullRefresh.refreshing = false;
        }, 1000);
    }

    onDetalle(item): void {
        alert('Mostrar los detalles del elemento ' + item);
    }

}
